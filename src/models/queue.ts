import { Schema, model } from 'mongoose';
import { queueMemberSchema } from './queueMember';

const queueSchema = new Schema({
  allowRejoin: { type: Boolean },
  name: { type: String, required: true },
  sequence: [queueMemberSchema],
});

const Queue = model('Queue', queueSchema);

export default Queue;
