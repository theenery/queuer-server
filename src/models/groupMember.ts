import { Schema, model } from 'mongoose';

export const groupMemberSchema = new Schema(
  {
    isModerator: { type: Boolean, default: false },
    notification: { type: Boolean },
    user: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
  },
  { _id: false }
);

const GroupMember = model('groupMember', groupMemberSchema);

export default GroupMember;
