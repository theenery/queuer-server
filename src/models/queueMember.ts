import { Schema, model } from 'mongoose';

export const queueMemberSchema = new Schema({
  joinMoment: { type: Date, default: Date.now },
  leaveMoment: { type: Date },
  shift: { type: Number, default: 0 },
  state: {
    type: String,
    enum: ['included', 'excluded', 'missed', 'prioritized'],
    default: 'included',
  },
  user: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
});

const QueueMember = model('QueueMember', queueMemberSchema);

export default QueueMember;
