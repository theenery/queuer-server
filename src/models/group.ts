import { Schema, model } from 'mongoose';
import { groupMemberSchema } from './groupMember';

const groupSchema = new Schema({
  allowRejoin: { type: Boolean, default: true },
  members: [groupMemberSchema],
  name: { type: String, required: true },
  owner: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
  queues: [{ type: Schema.Types.ObjectId, ref: 'Queue' }],
});

const Group = model('Group', groupSchema);

export default Group;
