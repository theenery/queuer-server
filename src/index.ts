import express from 'express';
import http from 'http';
import mongoose, { Types } from 'mongoose';
import { Server, Socket } from 'socket.io';
import dotenv from 'dotenv';

import Group from './models/group';
import GroupMember from './models/groupMember';
import Queue from './models/queue';
import QueueMember from './models/queueMember';
import User from './models/user';

// necessary tricks

interface CustomSocket extends Socket {
  user: Types.ObjectId;
}

declare global {
  interface String {
    toObjectId(): Types.ObjectId;
  }
}

String.prototype.toObjectId = function (this: string) {
  return Types.ObjectId.createFromHexString(this);
};

// init

dotenv.config();
const app = express();
const server = http.createServer(app);
const io = new Server(server);
const port = process.env.PORT!;

mongoose.connect(process.env.MONGO_DB!);
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error: '));
db.once('open', () => console.log('Connected to MongoDB'));

// io

io.use((rawSocket, next) => {
  const socket = rawSocket as CustomSocket;
  const user = socket.handshake.headers['user'] as string;
  if (!Types.ObjectId.isValid(user)) {
    console.log(`Client auth failed: ${user}`);
    return next(Error('Auth error'));
  }

  socket.user = user.toObjectId();
  return next();
});

io.on('connection', async (rawSocket) => {
  const socket = rawSocket as CustomSocket;

  console.log(`Client connected: socket = ${socket.id}, user = ${socket.user}`);

  const data = await fetchUserData(socket.user);
  data.groups.forEach((group) => socket.join(`/group/${group._id}`));
  socket.emit('fetchUserDataResponse', data);

  const internalError = (e: any) => {
    console.log(e);
    socket.emit('oops', 'Internal server error.');
  };

  socket.on('addGroupMember', async ({ group }) => {
    try {
      const data = await addGroupMember(group.toObjectId(), socket.user);
      io.in(`/group/${group}`).emit('addGroupMemberResponse', data);
      socket.emit('addGroupMemberResponse', data);
    } catch (e) {
      internalError(e);
    }
  });

  socket.on('createGroup', async ({ name }) => {
    try {
      const group = await createGroup(name, socket.user);
      socket.emit('createGroupResponse', { group });
    } catch (e) {
      internalError(e);
    }
  });

  socket.on('createQueue', async ({ group, name }) => {
    try {
      const queue = await createQueue(group.toObjectId(), name, socket.user);
      io.in(`/group/${group}`).emit('createQueueResponse', { group, queue });
    } catch (e) {
      internalError(e);
    }
  });

  socket.on('deleteGroup', async ({ group }) => {
    try {
      await deleteGroup(group.toObjectId(), socket.user);
      io.in(`/group/${group}`).emit('deleteGroupResponse', { group });
      io.socketsLeave(`/group/${group}`);
    } catch (e) {
      internalError(e);
    }
  });

  socket.on('deleteGroupMember', async ({ group, groupMember }) => {
    try {
      await deleteGroupMember(
        group.toObjectId(),
        groupMember.toObjectId(),
        socket.user
      );
      io.in(`/group/${group}`).emit('deleteGroupMemberResponse', {
        group,
        groupMember,
      });
    } catch (e) {
      internalError(e);
    }
  });

  socket.on('deleteQueue', async ({ group, queue }) => {
    try {
      await deleteQueue(group.toObjectId(), queue.toObjectId(), socket.user);
      io.in(`/group/${group}`).emit('deleteQueueResponse', { group, queue });
    } catch (e) {
      internalError(e);
    }
  });

  socket.on('disconnect', () => {
    console.log(
      `Client disconnected: socket = ${socket.id}, user = ${socket.user}`
    );
  });

  socket.on('memberErase', async ({ group, queue, queueMember }) => {
    try {
      await memberErase(
        group.toObjectId(),
        queue.toObjectId(),
        queueMember.toObjectId(),
        socket.user
      );
      io.in(`/group/${group}`).emit('memberEraseResponse', {
        group,
        queue,
        queueMember,
      });
    } catch (e) {
      internalError(e);
    }
  });

  socket.on('memberJoin', async ({ group, groupMember, queue }) => {
    try {
      const queueMember = await (
        await memberJoin(
          group.toObjectId(),
          groupMember.toObjectId(),
          queue.toObjectId(),
          socket.user
        )
      ).populate({ path: 'user' });
      io.in(`/group/${group}`).emit('memberJoinResponse', {
        group,
        queue,
        queueMember,
      });
    } catch (e) {
      internalError(e);
    }
  });

  socket.on('memberSetState', async ({ group, queue, queueMember, state }) => {
    try {
      const updated = await memberSetState(
        group.toObjectId(),
        queue.toObjectId(),
        queueMember.toObjectId(),
        state,
        socket.user
      );
      io.in(`/group/${group}`).emit('memberSetStateResponse', {
        group,
        leaveMoment: updated.leaveMoment,
        queue,
        queueMember,
        state: updated.state,
      });
    } catch (e) {
      internalError(e);
    }
  });

  socket.on('memberShift', async ({ group, queue, queueMember, shift }) => {
    try {
      await memberShift(group, queue, queueMember, shift, socket.user);
      socket.in(`/group/${group}`).emit('memberShiftResponse', {
        group,
        queue,
        queueMember,
        shift,
      });
    } catch (e) {
      internalError(e);
    }
  });

  socket.on('updateGroup', async ({ allowRejoin, group, name }) => {
    try {
      const updated = await updateGroup(
        allowRejoin,
        group.toObjectId(),
        name,
        socket.user
      );
      io.in(`/group/${group}`).emit('updateGroupResponse', {
        allowRejoin: updated.allowRejoin,
        group,
        name: updated.name,
      });
    } catch (e) {
      internalError(e);
    }
  });

  socket.on('updateGroupNotification', async ({ group, notification }) => {
    try {
      const updated = await updateGroupNotification(
        group.toObjectId(),
        notification,
        socket.user
      );
      socket.emit('updateGroupNotificationResponse', {
        group,
        notification: updated.members.find((m) => m.user.equals(socket.user))
          ?.notification,
        user: socket.user,
      });
    } catch (e) {
      internalError(e);
    }
  });

  socket.on('updateModerator', async ({ group, groupMember, isModerator }) => {
    try {
      await updateModerator(
        group.toObjectId(),
        groupMember.toObjectId(),
        isModerator,
        socket.user
      );
      io.in(`/group/${group}`).emit('updateModeratorResponse', {
        group,
        groupMember,
        isModerator,
      });
    } catch (e) {
      internalError(e);
    }
  });

  socket.on('updateQueue', async ({ allowRejoin, group, name, queue }) => {
    try {
      const updated = await updateQueue(
        allowRejoin,
        group.toObjectId(),
        name,
        queue,
        socket.user
      );
      io.in(`/group/${group}`).emit('updateQueueResponse', {
        allowRejoin: updated.allowRejoin,
        group,
        name: updated.name,
        queue,
      });
    } catch (e) {
      internalError(e);
    }
  });

  socket.on('updateUserName', async ({ name }) => {
    try {
      await updateUserName(name, socket.user);
      socket.emit('updateUserNameResponse', { name });
    } catch (e) {
      internalError(e);
    }
  });
});

server.listen(port, () => {
  console.log(`Server is running at port ${port}`);
});

// mongo

async function addGroupMember(_group: Types.ObjectId, _user: Types.ObjectId) {
  if (await Group.exists({ _id: _group, members: { user: _user } }))
    throw Error('User is already in group');

  const groupMember = new GroupMember({ user: _user });
  await Group.findByIdAndUpdate(_group, {
    $push: { members: groupMember },
  });
  const group = await Group.findById(_group)
    .populate({ path: 'members.user' })
    .populate({ path: 'queues', populate: { path: 'sequence.user' } });

  await groupMember.populate('user');
  return { group, groupMember };
}

async function createGroup(_name: string, _user: Types.ObjectId) {
  const groupData = {
    members: [{ isModerator: true, user: _user }],
    name: _name,
    owner: _user,
  };

  const group = await Group.create(groupData);

  return await Group.findById(group._id).populate({ path: 'members.user' });
}

async function createQueue(
  _group: Types.ObjectId,
  _name: string,
  _user: Types.ObjectId
) {
  const group = await findGroupAsOwner(_group, _user);

  const queueData = { name: _name };

  const queue = await Queue.create(queueData);
  await group.updateOne({ $push: { queues: queue._id } });
  return queue;
}

async function deleteGroup(_group: Types.ObjectId, _user: Types.ObjectId) {
  const group = await findGroupAsOwner(_group, _user);
  await Queue.deleteMany({ _id: { $in: group.queues } });

  return await group.deleteOne();
}

async function deleteGroupMember(
  _group: Types.ObjectId,
  _groupMember: Types.ObjectId,
  _user: Types.ObjectId
) {
  const group = await Group.findById(_group);
  if (group == null) throw Error(`Group with id = ${_group} is not found`);
  if (!group.members.find((m) => m.user.equals(_groupMember)))
    throw Error(
      `Member with id = ${_groupMember} is not found in group = ${_group}`
    );
  if (_groupMember.equals(group.owner))
    throw Error('The owner cannot be removed');
  if (!(_groupMember.equals(_user) || group.owner.equals(_user)))
    throw Error(`Access violation: owner = ${group.owner}, user = ${_user}`);
  return await group.updateOne({ $pull: { members: { user: _groupMember } } });
}

async function deleteQueue(
  _group: Types.ObjectId,
  _queue: Types.ObjectId,
  _user: Types.ObjectId
) {
  const group = await findGroupAsOwner(_group, _user);

  await group.updateOne({ $pull: { queues: _queue } });
  return await Queue.findByIdAndDelete(_queue);
}

async function fetchUserData(_id: Types.ObjectId) {
  const groups = await Group.find({ 'members.user': _id })
    .populate('members.user')
    .populate({ path: 'queues', populate: { path: 'sequence.user' } });
  const user = await User.findById(_id);
  return { user, groups };
}

async function findGroupAsOwner(_group: Types.ObjectId, _user: Types.ObjectId) {
  const group = await Group.findById(_group);
  if (group == null) throw Error(`Group with id = ${_group} is not found`);
  if (!group.owner.equals(_user))
    throw Error(`Access violation: owner = ${group.owner}, user = ${_user}`);
  return group;
}

async function findQueueMemberIn(
  _group: Types.ObjectId,
  _queue: Types.ObjectId,
  _queueMember: Types.ObjectId,
  _user: Types.ObjectId
) {
  const group = await Group.findOne(
    { _id: _group, queues: _queue },
    {},
    { select: 'members' }
  );

  if (!group) throw Error(`Group with id = ${_group} is not found`);
  if (!group.members.some((m) => m.user.equals(_user)))
    throw Error(`Access violation: group = ${group._id}, user = ${_user}`);

  const queue = await Queue.findById(_queue, {}, { select: 'sequence' });

  if (!queue) throw Error(`Queue with id = ${_queue} is not found`);

  const queueMember = queue.sequence.find((m) => m._id?.equals(_queueMember));

  if (!queueMember)
    throw Error(`QueueMember with id = ${_queueMember} is not found`);
  if (
    !queueMember.user.equals(_user) &&
    !group.members.some((m) => m.user.equals(_user) && m.isModerator)
  )
    throw Error(`Access violation: group = ${group._id}, user = ${_user}`);

  return { group, queue, queueMember };
}

async function memberErase(
  _group: Types.ObjectId,
  _queue: Types.ObjectId,
  _queueMember: Types.ObjectId,
  _user: Types.ObjectId
) {
  await findQueueMemberIn(_group, _queue, _queueMember, _user);

  return await Queue.findByIdAndUpdate(
    _queue,
    { $pull: { sequence: { _id: _queueMember } } },
    { new: true }
  );
}

async function memberJoin(
  _group: Types.ObjectId,
  _groupMember: Types.ObjectId,
  _queue: Types.ObjectId,
  _user: Types.ObjectId
) {
  const group = await Group.findOne(
    { _id: _group, queues: _queue },
    {},
    { select: 'allowRejoin members' }
  );

  if (!group) throw Error(`Group with id = ${_group} is not found`);
  if (!group.members.some((m) => m.user.equals(_groupMember)))
    throw Error(
      `Access violation: group = ${group._id}, member = ${_groupMember}`
    );
  if (
    !_groupMember.equals(_user) &&
    !group.members.some((m) => m.user.equals(_user) && m.isModerator)
  )
    throw Error(`Access violation: group = ${group._id}, user = ${_user}`);

  const queue = await Queue.findById(
    _queue,
    {},
    { select: 'allowRejoin sequence' }
  );

  if (!queue) throw Error(`Queue with id = ${_queue} is not found`);

  const allowRejoin = queue.allowRejoin ?? group.allowRejoin;
  const queueMember = new QueueMember({ user: _groupMember });

  const filterQuery = allowRejoin
    ? { _id: _queue }
    : { _id: _queue, 'sequence.user': { $ne: _groupMember } };

  const updated = await Queue.findOneAndUpdate(
    filterQuery,
    { $push: { sequence: queueMember } },
    { new: true }
  );
  if (!updated) throw Error(`Failed to update queue with id ${_queue}.`);
  return queueMember;
}

async function memberSetState(
  _group: Types.ObjectId,
  _queue: Types.ObjectId,
  _queueMember: Types.ObjectId,
  _state: string,
  _user: Types.ObjectId
) {
  await findQueueMemberIn(_group, _queue, _queueMember, _user);

  const setQuery =
    _state == 'excluded'
      ? {
          $set: {
            'sequence.$[member].state': _state,
            'sequence.$[member].leaveMoment': Date.now(),
          },
        }
      : { $set: { 'sequence.$[member].state': _state } };

  const updated = await Queue.findOneAndUpdate({ _id: _queue }, setQuery, {
    arrayFilters: [{ 'member._id': _queueMember }],
    new: true,
  });
  if (!updated) throw Error(`Failed to update queue with id ${_queue}.`);
  return updated.sequence.find((m) => m._id!.equals(_queueMember))!;
}

async function memberShift(
  _group: Types.ObjectId,
  _queue: Types.ObjectId,
  _queueMember: Types.ObjectId,
  _shift: number,
  _user: Types.ObjectId
) {
  let { queue, queueMember } = await findQueueMemberIn(
    _group,
    _queue,
    _queueMember,
    _user
  );

  function _recalculateOrder() {
    /* function from app code */
    let modifiable = queue.sequence;
    modifiable.sort(
      (m1, m2) => m1.joinMoment.getTime() - m2.joinMoment.getTime()
    );
    for (let i = modifiable.length - 1; i >= 0; i--) {
      let shift = modifiable[i].shift;
      for (let j = 0; j < shift && i + j < modifiable.length - 1; j++) {
        const swap = modifiable[i + j];
        modifiable[i + j] = modifiable[i + j + 1];
        modifiable[i + j + 1] = swap;
      }
    }
  }
  _recalculateOrder();

  const index = queue.sequence.indexOf(queueMember);
  if (_shift > 0) {
    for (let i = 0; i < _shift && index + i + 1 < queue.sequence.length; i++) {
      if (
        queue.sequence[index + i + 1].shift > 0 &&
        queue.sequence[index + i + 1].shift > queueMember.shift
      ) {
        queue.sequence[index + i + 1].shift--;
      } else {
        queueMember.shift++;
      }
    }
  } else {
    for (let i = 0; i > _shift && index + i > 0; i--) {
      if (
        queueMember.shift > 0 &&
        queueMember.shift > queue.sequence[index + i - 1].shift
      ) {
        queueMember.shift--;
      } else {
        queue.sequence[index + i - 1].shift++;
      }
    }
  }
  //_recalculateOrder();

  const bulkUpdates = queue.sequence.map((data) => ({
    updateOne: {
      filter: { _id: _queue, 'sequence.user': data.user },
      update: { $set: { 'sequence.$.shift': data.shift } },
    },
  }));

  const updated = await Queue.bulkWrite(bulkUpdates);
  if (!updated) throw Error(`Failed to update queue with id ${_queue}.`);
}

async function updateGroup(
  _allowRejoin: boolean,
  _group: Types.ObjectId,
  _name: string,
  _user: Types.ObjectId
) {
  const group = await findGroupAsOwner(_group, _user);

  const updated = await Group.findByIdAndUpdate(
    group._id,
    {
      $set: { allowRejoin: _allowRejoin, name: _name },
    },
    { new: true }
  );
  if (!updated) throw Error(`Failed to update group with id ${_group}.`);
  return updated;
}

async function updateModerator(
  _group: Types.ObjectId,
  _groupMember: Types.ObjectId,
  _isModerator: boolean,
  _user: Types.ObjectId
) {
  const group = await findGroupAsOwner(_group, _user);
  if (!group.members.find((m) => m.user.equals(_groupMember)))
    throw Error(
      `Member with id = ${_groupMember} is not found in group = ${_group}`
    );
  await group.updateOne(
    { $set: { 'members.$[member].isModerator': _isModerator } },
    { arrayFilters: [{ 'member.user': _groupMember }] }
  );
}

async function updateGroupNotification(
  _group: Types.ObjectId,
  _notification: Boolean,
  _user: Types.ObjectId
) {
  const updated = await Group.findOneAndUpdate(
    { _id: _group },
    {
      $set: { 'members.$[member].notification': _notification },
    },
    { arrayFilters: [{ 'member.user': _user }], new: true }
  );
  if (!updated) throw Error(`Failed to update group with id ${_group}.`);
  return updated;
}

async function updateQueue(
  _allowRejoin: boolean,
  _group: Types.ObjectId,
  _name: string,
  _queue: Types.ObjectId,
  _user: Types.ObjectId
) {
  const group = await Group.exists({
    _id: _group,
    queues: _queue,
    owner: _user,
  });

  if (!group)
    throw Error(
      `Group with id = ${_group}, queue = ${_queue}, owner = ${_user} is not found`
    );

  const updated = await Queue.findByIdAndUpdate(
    _queue,
    {
      $set: { allowRejoin: _allowRejoin, name: _name },
    },
    { new: true }
  );
  if (!updated) throw Error(`Failed to update queue with id ${_queue}.`);
  return updated;
}

async function updateUserName(_name: string, _user: Types.ObjectId) {
  await User.findByIdAndUpdate(_user, { name: _name });
}
